---
title: "Untitled"
author: "rdfleay"
date: "27/11/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:
```{r DataLoad, warning=FALSE, message=FALSE}
# restart Load2
#getwd()
setwd("/home/rdfleay/Documents/Study/MER/ryan-master-thesis/ryan-master-thesis/Scripts")

source("DataLoad_IW_CBay.R") # ATT !! do not add DataLoad3D.R here, generates errors through 3DMod.deciday
```

```{r}
# restart Load3
PairCol <- brewer.pal(12, "Paired")
SigCol <- colorRampPalette(PairCol) 

#NegN2 <- brewer.pal(colors=c("magenta", "white"), values = c(-1, -0))
#SigColNegN2 <- colorRampPalette(PairCol, Neg) 

#color_ramp -> ColorRamp(colors=("white", "blue"), values=(0,10))
#Neg<-scale_fill_gradientn(colours=c("magenta"), values=rescale(c(T1a, -1,0-)))

#SA.col <- colorRampPalette(c("navy","steelblue2","slateblue2","lightblue2","white"))
#Sol.col <- colorRampPalette(c("wheat","wheat1","wheat2","wheat3","wheat4"))
#Sat.col <- colorRampPalette(c("lightgoldenrod","lightgoldenrod1","lightgoldenrod2","lightgoldenrod3","lightgoldenrod4"))
```
Sigma/O2sat:Depth
```{r}

# O2sat:Depth

T1 <- dlply( df, .(decydayF),function(dsub){
  #rr<<-dsub
  ## !! ART - Added the following 'try_default' 
  ## !! to return a column of nans when this particular time slot was not sampled !! ##
  app<- try_default(
    #approx(dsub$depth, dsub$O2sat, stddepth)$y,
    oce::interpBarnes(x = dsub$O2sat, y = stddepth, z = dsub$depth, xgl = 66, ygl = 200),
    #interpolated.temperature = interpBarnes(x = all.tb$day.year, y = all.tb$year, z = all.tb$sst, xgl = 366, ygl = 21)
    rep(NA,length(stddepth)),quiet = TRUE)
  ##  ## ## 
   if(FALSE){
    # print(unique(dsub$decydayF))
    plot(dsub$depth, dsub$O2sat) 
     lines(stddepth,app)
     Sys.sleep(0.25)
   }
  return(app)
}, .inform = T, .drop = FALSE)

T1a<-t(do.call(cbind,T1))

 at <- seq(98.67,0.03,length.out = 50)
 
 timeseq <- seq(from=1,to=length(stddeciday_mid),by=8*12)
 
# full column 
T1b <- levelplot(T1a,
                column.values =  stddepth,
                row.values = stddeciday_mid,
                col.regions=SigCol, #oce.colorsOxygen(200), #rainbow(60), #oce.colorsSalinity
                ylim = c(200,30),
                xlim = c(0,70),
                ylab=list(expression(paste("[m]")), cex=0.8), 
                xlab=list('Sampling date', cex=0.8) ,
                cuts=50,
                main = list(bquote(paste("DO (O"[2],"sat) : Depth (dbar) TS")),side=1,line=0.5),
                xlab.top='Black Sea, NE continental slope, 2016', #cente AC
                legend=list(bottom=list(fun= grid::textGrob(expression("[%]"),
                                                            x=1.08, y=5.0), cex=0.8),       # ??
                            colorkey=list(reverse=TRUE)),
                scales=list(x=list(at=stddeciday_mid[timeseq], rot=45,# Correct? data for 6 March...??
                                   labels=format(stddeciday_mid_ltime[timeseq], format = '%d-%b'))),
                panel = function(...){
                                panel.levelplot(...)
                                panel.abline(h = list(65,90), lty = "dotted", col = "black", lwd = 0.8)
                            panel.abline(v = list(22,38,50), lty = "dotted", col = "black", lwd = 0.8)
                                },
                )
update(T1b, aspect=0.418) #0.618


# sigma:depth

T1 <- dlply( df, .(decydayF),function(dsub){
  #rr<<-dsub
  ## !! ART - Added the following 'try_default' 
  ## !! to return a column of nans when this particular time slot was not sampled !! ##
  app<- try_default(
    approx(dsub$depth, dsub$sigma, stddepth)$y,
    rep(NA,length(stddepth)),quiet = TRUE)
  ##  ## ## 
   if(FALSE){
    # print(unique(dsub$decydayF))
    plot(dsub$depth, dsub$sigma) 
     lines(stddepth,app)
     Sys.sleep(0.25)
   }
  return(app)
}, .inform = T, .drop = FALSE)

T1a<-t(do.call(cbind,T1))

 at <- seq(13,16.8,length.out = 50)
 
 timeseq <- seq(from=1,to=length(stddeciday_mid),by=8*12)
 
 # full -c                    # COLORS
 
T1b <- levelplot(T1a,
                column.values =  stddepth,
                row.values = stddeciday_mid,
                col.regions=SigCol, #topo.colors(400), #oce.colorsSalinity
                ylim = c(200,30),
                xlim = c(0,70),
                ylab=list(expression(paste("[m]")), cex=0.8), 
                xlab=list('Sampling date', cex=0.8) ,
                cuts=50,
                main = list('Sigma (σ) : Depth (dbar) TS',side=1,line=0.5), #μmol L −1, mmolNm-3
                xlab.top='BS NE, 2016', #cente AC
                legend=list(bottom=list(fun= grid::textGrob(expression("[kg m"^"-3"*"]"),
                                                                        x=1.08, y=4.0), cex=0.8),                                                        colorkey=list(reverse=TRUE)),
                at=at,
                scales=list(x=list(at=stddeciday_mid[timeseq], rot=45,# 
                                   labels=format(stddeciday_mid_ltime[timeseq], format = '%d-%b'))),
                panel = function(...){
                                panel.levelplot(...)
                                panel.abline(h = list(65,90), lty = "dotted", col = "black", lwd = 0.8)
                            panel.abline(v = list(22,38,50), lty = "dotted", col = "black", lwd = 0.8)
                                },
                )
update(T1b, aspect=0.418) #0.618

#O2:sigma

T1 <- dlply( df, .(decydayF),function(dsub){
  #rr<<-dsub
  ## !! ART - Added the following 'try_default' 
  ## !! to return a column of nans when this particular time slot was not sampled !! ##
  app<- try_default(
    approx(dsub$sigma, dsub$O2sat, stdsigma)$y,
    rep(NA,length(stdsigma)),quiet = TRUE)
  ##  ## ## 
   if(FALSE){
    # print(unique(dsub$decydayF))
    plot(dsub$sigma, dsub$O2sat) 
     lines(stdsigma,app)
     Sys.sleep(0.25)
   }
  return(app)
}, .inform = T, .drop = FALSE)

T1a<-t(do.call(cbind,T1))

 at <- seq(98.67,0.03,length.out = 50)
 
 timeseq <- seq(from=1,to=length(stddeciday_mid),by=8*12)

# full -c  ,SigCol alternate colour

T1f <- levelplot(T1a,
                column.values =  stdsigma,
                row.values = stddeciday_mid,
                col.regions=SigCol, oce.colorsOxygen(200), #rainbow(60), #oce.colorsSalinity
                ylim = c(16,13.6),
                xlim = c(0,70),
                ylab=list(expression(paste("[kg m"^"-3"*"]")), cex=0.8),# expression(paste("Irrig area, km"^"2"))
                xlab=list('Sampling date', cex=0.8) ,
                cuts=50,
                main = list(bquote(paste("DO (O"[2],"sat) : Sigma (σ) TS")),side=1,line=0.5),
                xlab.top='Black Sea, NE continental slope, 2016', #cente AC
                legend=list(bottom=list(fun= grid::textGrob(expression("[%]"),
                                                            x=1.08, y=5.0), cex=0.8),                                                        colorkey=list(reverse=TRUE)),
                at=at,
                scales=list(x=list(at=stddeciday_mid[timeseq], rot=45,
                                   labels=format(stddeciday_mid_ltime[timeseq], format = '%d-%b'))),
                panel = function(...){
                                panel.levelplot(...)
                                panel.abline(v = list(22,38,50), lty = "dotted", col = "black", lwd = 0.8)
                                #panel.abline(h = 14.5)
                },
                )
update(T1f, aspect=0.418) #0.618
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
